// region Import

var fs       = require('fs'),
    url      = require('url'),
    config   = require('../config/'),
    jwt      = require('jwt-simple'),
    jsdom    = require('jsdom'),
    jquery   = require('jquery');


// endregion

// region Inner Methods

/**
 * Middleware that reads the static html files
 * @param req
 * @param res
 * @param next
 */
function authorizeParts(req, res, next){
    var root = __dirname + '/../public',
        parts = req.url.split('.'),
        fileType = parts[parts.length - 1];

    if(fileType === 'html'){
        fs.readFile(root + req.url, 'utf8', function(err, html){
            if (err) {
                return console.log(err);
            }

            jsdom.env(html, _removeUnAuthorizedParts.bind(null, req, res));
        });
    } else{
        next();
    }
}

/**
 * Removes the parts of the html that this user is not authorized to view
 * @param window
 * @private
 */
function _removeUnAuthorizedParts(req, res, errors, window){
    var $ = jquery(window),
        $restrictedParts = $('.restricted');

    $restrictedParts.each(function(){
        var $el = $(this),
            data = $el.data();

        eval("var accessRights = " + data.accessRights);

        // if claims not matched remove
        if(!_checkElementAccessibility(req, accessRights)){
            $el.remove();
        }

        // clean up the element e.g. remove the data-access-rights attribute
        $el.removeAttr('data-access-rights');
        $el.removeClass('restricted');
    });

    res.send($('html').html());
}

/**
 * Checks if the given set of access rights correspond to user's set of claims
 * @param accessRights
 * @private
 */
function _checkElementAccessibility(req, accessRights){
    // iterate through all the element's access rights
    for(var accessRight in accessRights){
        var claimValues = accessRights[accessRight],
            userCorespondingClaims = _getUserClaim(req, accessRight),

            commonClaims = userCorespondingClaims.filter(function(item){
                return claimValues.indexOf(item.value) !== -1;
            });

        // element claim and user's claim for don't match
        if(!commonClaims.length){
            return false;
        }
    }

    return true;
}

/**
 * Returns the value for the given claim type
 * @param req
 * @param claimType
 * @private
 */
function _getUserClaim(req, claimType){
    var authHdr = req.headers['authorization'];

    if(authHdr){
        var encodedToken = authHdr.split(' ')[1],
            token = jwt.decode(encodedToken, config.get('security:jwt_secret'));

        return token.claims.filter(function(claim){ return claim.type === claimType; });
    } else {
        return [];
    }
}

// endregion

// region Export

exports.authorizeParts = authorizeParts;

// endregion