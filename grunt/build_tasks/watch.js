module.exports = {
    options: {
        livereload: true,
        nospawn: true
    },

    dev: {
        files: ['<%= appDir %>/**/*.js', '<%= cssDir %>/app/**/*.scss', '<%= appDir %>/**/*.scss'],
        tasks: ['watchTasks']
    }
};