var config = require('../config');

module.exports = {
    dev: {
        files: [{
            expand: true,
            src: config.copyFiles.foundationFonts,
            dest: '<%= distDir %>/css/',
            flatten: true,
            filter: 'isFile'
        }]
    }
};