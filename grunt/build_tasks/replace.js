module.exports = {
    dev: {
            options: {
                patterns: [
                    {
                        match: 'TOKEN_URL',
                        replacement: 'http://localhost:9090/oauth/token/'
                    },
                    {
                        match: 'SIGNUP_URL',
                        replacement: 'http://localhost:9090/signup'
                    },
                    {
                        match: 'SERVICE_ENDPOINT',
                        replacement: 'http://localhost:9999/api/v1/'
                    },
                    {
                        match: 'IMAGE_URL',
                        replacement: 'http://localhost:9999/api/v1/images/'
                    },
                    {
                        match: 'PRODUCT_IMAGE_URL',
                        replacement: 'http://localhost:9999/api/v1/products/:id/image'
                    }
                ]
        },

        files: [
            {expand: true, flatten: true, src: ['<%= distDir %>/bundle.js'], dest: '<%= distDir %>'}
        ]
    },

    openshift: {
        options: {
            patterns: [
                {
                    match: 'TOKEN_URL',
                    replacement: 'http://localhost:9090/oauth/token/'
                },
                {
                    match: 'SERVICE_ENDPOINT',
                    replacement: 'http://api-3dworld.rhcloud.com/api/v1/'
                }
            ]
        },

        files: [
            {expand: true, flatten: true, src: ['<%= distDir %>/bundle.js'], dest: '<%= distDir %>'}
        ]
    }
};