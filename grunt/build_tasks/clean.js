module.exports = {
    options: {
        force: true
    },
    dist: ['<%= distDir %>/*', '!<%= distDir %>/sass/**', '!<%= distDir %>/fonts/**'],
    sass: ['<%= distDir %>/sass/**']
};
