﻿
var bundles = {
    js: {
        vendor: [
            '<%= libsDir %>/angular/angular.js',
            '<%= libsDir %>/angular-animate/angular-animate.js',
            '<%= libsDir %>/angular-ui-router/release/angular-ui-router.js',
            '<%= libsDir %>/breezejs/breeze.debug.js',
            '<%= libsDir %>/breezejs/labs/breeze.angular.js',
            '<%= libsDir %>/alertify.js/lib/alertify.js',
            '<%= libsDir %>/accounting/accounting.js',
            '<%= libsDir %>/event/event.js',
            '<%= libsDir %>/magnifier/Magnifier.js',
            '<%= libsDir %>/ngDialog/js/ngDialog.js',
            '<%= libsDir %>/angular-loading-bar/build/loading-bar.js'
        ],

        app: [
            '<%= appDir %>/config/http-provider-config.js',
            '<%= appDir %>/config/routes.js',
            '<%= appDir %>/app.js'
        ]
    },

    css: {
        vendor: [
            '<%= libsDir %>/alertify.js/themes/alertify.core.css',
            '<%= libsDir %>/alertify.js/themes/alertify.default.css',
            '<%= cssDir %>/foundation-icons/css/foundation-icons.css',
            '<%= libsDir %>/magnifier/magnifier.css',
            '<%= libsDir %>/ngDialog/css/ngDialog.css',
            '<%= libsDir %>/ngDialog/css/ngDialog-theme-default.css',
            '<%= libsDir %>/angular-loading-bar/build/loading-bar.css'
        ],

        app: [
            '<%= distDir %>/sass/app.css'
        ]
    },

    sass: {
        vendor: {
        },

        app: {
            '<%= distDir %>/app.css': '<%= cssDir %>/app/app.scss'
        }
    }
};

bundles.testScripts = [
    '<%= libsDir %>/angular/angular.min.js',
    '<%= libsDir %>/angular/angular-ui-router/release/angular-ui-router.min.js',
    '<%= jsDir %>/breezejs/breeze.debug.js'
].concat(bundles.js);

var copyFiles = {
    foundationFonts: '<%= cssDir %>/fi-social-facebook'
};

var banner = '/*!\n' +
    ' * <%= pkg.name %>\n' +
    ' * <%= pkg.title %>\n' +
    ' * <%= pkg.url %>\n' +
    ' * @author <%= pkg.author %>\n' +
    ' * @version <%= pkg.version %>\n' +
    ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
    ' */\n';

module.exports.bundles = bundles;
module.exports.banner = banner;
module.exports.copyFiles = copyFiles;