var express  = require('express'),
    router   = express.Router(),
    utils    = require('../utils/httpRequest'),
    config   = require('../config/');

var  METADATA_ENDPOINT = '/api/v1/breeze/metadata',
    _devMode = config.get("dev"),
    _breezeMetadata;

/* GET home page. */
router.get('/', function(req, res) {
    _loadMetada(req, res);
});

// Support Html5 mode
router.all('/*', function(req, res) {
    _loadMetada(req, res);
});

/***
 * Loads the breeze metadata
 * @private
 */
var _loadMetada = function(req, res){
    if(_breezeMetadata){
        res.render('index', { dev: _devMode, breezeMetadata: JSON.stringify(_breezeMetadata) });

        return;
    }

    var options = {
        host: config.get('api_endpoint:host'),
        path: METADATA_ENDPOINT,
        port: config.get('api_endpoint:port')
    };

    utils.request(options)
        .then(function(breezeMetadata){
            _breezeMetadata = JSON.parse(breezeMetadata);
            res.render('index', { dev: _devMode, breezeMetadata: breezeMetadata });
        })
        .fail(function(err){
            console.log("Got error while retrieving breeze metadata: " + err.message);
            res.send(500);
        });
};

module.exports = router;
