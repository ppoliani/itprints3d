var webpack = require('gulp-webpack'),
    replace = require('gulp-replace-task'),
    concat  = require('gulp-concat');

// config
var webpackOptions = {

    output: {
        filename: 'bundle.js'
    },

    externals: {
        'angular': 'angular'
    },

    devtool: '#source-map'
};

var replacePatterns = [{
        match: 'TOKEN_URL',
        replacement: 'http://localhost:9090/oauth/token/'
    }, {
        match: 'SIGNUP_URL',
        replacement: 'http://localhost:9090/signup'
    }, {
        match: 'SERVICE_ENDPOINT',
        replacement: 'http://localhost:9999/api/v1/'
    }, {
        match: 'IMAGE_URL',
        replacement: 'http://localhost:9999/api/v1/images/'
    }, {
        match: 'PRODUCT_IMAGE_URL',
        replacement: 'http://localhost:9999/api/v1/products/:id/image'
    }
];

module.exports = function(gulp, config){
    gulp.task('scripts:dev', function(){
        return gulp.src(config.appDir + '/core/app.js')
            .pipe(webpack(webpackOptions))
            .pipe(replace({ patterns: replacePatterns }))
            .pipe(gulp.dest(config.distDir + '/'));
    });

    gulp.task('concat:dev', function(){
        return gulp.src(config.bundles.js.vendor)
            .pipe(concat('vendor.js'))
            .pipe(gulp.dest(config.distDir + '/'));
    });
};