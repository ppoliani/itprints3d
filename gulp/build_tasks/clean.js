var clean = require('gulp-clean');

module.exports = function(gulp, config){
    gulp.task('clean:dist', function () {
        return gulp.src([config.distDir + '/*', '!' + config.distDir + '/sass/**', '!' + config.distDir + '/fonts/**'], {read: false})
            .pipe(clean({ force: true }));
    });

    gulp.task('clean:sass', function () {
        return gulp.src(config.distDir + '/sass/**', {read: false})
            .pipe(clean({ force: true }));
    });
};