module.exports = function(gulp, config){
    gulp.task('copy', function(){
        gulp.start('copy:fonts');
    });

    gulp.task('copy:fonts', function () {
        return gulp.src([config.cssDir + '/foundation-icons/fonts/**/*.*'])
            .pipe(gulp.dest(config.distDir + '/fonts'));
    });
};