/**
 * Under construction app
 */
 (function(){
     'use strict';

     angular.module('underConstruction', ['ngRoute'])
         .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
             $routeProvider.
                 when('/', {
                     templateUrl: 'under-construction/under-construction.html',
                     controller: 'underConstructionCtrl'
                 })

                 .otherwise({
                     redirectTo: '/'
                 });

             $locationProvider.html5Mode(true);
         }])
         .controller('underConstructionCtrl', ['$scope', '$http', function ($scope, $http){
             $scope.emailSubmitted = false;

             $scope.emailRegistered = false;

             $scope.email;

             $scope.saveEmail = function ($invalid){
                if(!$invalid && !$scope.emailSubmitted){
                    $scope.emailSubmitted = true;

                    $http.post('http://localhost:9999/api/v1/email', { email: $scope.email })
                        .success(function(){
                            $scope.emailRegistered = true;
                        })
                        .catch(function(err){
                            console.error('Could not register the given email ' + err);
                        });
                }
            };
         }]);
 })();
