(function(){
    'use strict';

    function priceFilter(accounting){
        return function(value){
            return accounting.formatMoney(value, '£', 2, ',', '.');
        };
    }

    // region Exports

    module.exports = {
        name: 'priceFormat',
        type: 'filter',
        filter: ['accounting', priceFilter]
    };

    // endregion
})();