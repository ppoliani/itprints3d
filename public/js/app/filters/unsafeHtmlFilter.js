/**
 * Inserts unsafe html
 */
(function(){
    'use strict';

    function unsafeHtmlFilter($sce){
        return function(value){
            return $sce.trustAsHtml(value);
        };
    }

    // region Exports

    module.exports = {
        name: 'unsafeHtml',
        type: 'filter',
        filter: [
            '$sce',
            unsafeHtmlFilter
        ]
    };

    // endregion
})();