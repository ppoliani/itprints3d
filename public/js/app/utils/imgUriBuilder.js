/**
 * Image uri builder
 */
 (function(){
    'use strict';

    function imgUriBuilderService(IMAGE_URL, PRODUCT_IMAGE_URL){

        // region Inner Methods

        function buildImageUri(imageId, width, height){
            return _getUriFromId(IMAGE_URL, imageId)  + '?width=' + width + '&height=' + height;
        }

        function buildProductImageUri(imageId, width, height, category){
            return _getUriFromId(PRODUCT_IMAGE_URL, imageId)  + '?width=' + width + '&height=' + height + '&product=' + category;
        }

        function _getUriFromId(baseUri, id){
            return baseUri.indexOf(':id') !== -1
                ? baseUri.replace(':id', id)
                : baseUri + id;
        }

        // endregion

        // region Public API

        return {
            buildImageUri: buildImageUri,
            buildProductImageUri: buildProductImageUri
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'imgUriBuilderService',
        type: 'factory',
        service: [
            'IMAGE_URL',
            'PRODUCT_IMAGE_URL',
            imgUriBuilderService
        ]
    };

    // endregion

 })();