/**
 * Pubsub service
 */
(function () {
    'use strict';

    function pubSubService() {

        // region Deps

        var hubjs = require('hubjs')();

        // endregion

        // region Inner Methods

        /**
         * Subscribe to the given topic
         * @param topic
         */
        function on(topic, eventListener){
            hubjs.on(topic, eventListener);
        }

        /**
         * Publishes data to the subscribers of the given topic
         * @param topic
         * @param data
         */
        function publish(topic, data){
            hubjs.emit(topic, data);
        }

        /**
         * Unsubscribes the given subscription
         * @param topic
         * @param subscription
         */
        function off(topic, subscription){
            throw 'Not Implemented';
        }

        // endregion

        // region Public API

        return {
            on: on,
            publish: publish,
            off: off
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'pubSubService',
        type: 'factory',
        service: [pubSubService]
    };

    // endregion

})();