/**
 * The service that deals with auth-related stuff
 */
(function() {
    'use strict';

    // region Service

    function authService($http, $window, $location, TOKEN_URL, SIGNUP_ENDPOINT, pubSub, logger){

        // region Inner Methods

        /**
         * Returns true if user is logged in
         */
        function isLoggedIn(){
            return $window.sessionStorage.access_token;
        }

        /***
         * Checks if user is logged-in; in which case redirect to the previous url
         * @private
         */
        function checkIfLoggedIn(){
            logger.info('Checking if use logged-in');

            if(isLoggedIn()){
                _redirectAfterSuccess();
            }
        }

        /**
         * Will try to login with the given credentials
         */
        function login(username, password){
            var data = 'grant_type=password&username=' + username + '&password=' + password + '&scope=email';

            $http({
                    method: 'POST',
                    url: TOKEN_URL,
                    data: data,
                    headers: {
                        Authorization: 'Basic YWJjMTIzOnNzaHNlY3JldA==',
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .then(function(response){
                    logger.info("Successfully logged in");

                    $window.sessionStorage.access_token = response.data['access_token'];
                    $window.sessionStorage.user_info = JSON.stringify(response.data['user_info']);

                    pubSub.publish('login:success');

                    if($location.url().indexOf('/login') !== -1){
                        _redirectAfterSuccess();
                    }
                })
                .catch(function(err){
                    logger.error("Could not login: " + err.data);
                });
        }

        function signup(username, email, password){
            var data = {
                username: username,
                email: email,
                password: password
            };

            $http({
                method: 'POST',
                url: SIGNUP_ENDPOINT,
                data: data
                })
                .then(function(response){
                    if(!response.data.message){
                        logger.info("Successful registration");
                    }
                    else {
                        logger.error("Could not complete registration process: " + response.data.message);
                    }
                })
                .catch(function(){
                    logger.error("Could not complete registration process.");
                });
        }

        /**
         * Returns the info for the logged in user
         */
        function getUserInfo(){
            return $window.sessionStorage.user_info
                && JSON.parse($window.sessionStorage.user_info);
        }

        function _redirectAfterSuccess(){
            $location.path('/');
        }

        // endregion

        return {
            login: login,
            signup: signup,
            isLoggedIn: isLoggedIn,
            checkIfLoggedIn: checkIfLoggedIn,
            getUserInfo: getUserInfo
        };
    }

    // endregion

    // region CommonJS

    module.exports = {
        name: 'authService',
        type: 'factory',
        service: [
            '$http',
            '$window',
            '$location',
            'TOKEN_URL',
            'SIGNUP_ENDPOINT',
            'pubSubService',
            'logger',
            authService]
    };

    // endregion
})();