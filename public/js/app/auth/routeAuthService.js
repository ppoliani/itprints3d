/**
 * A service that decides which routes are allowed for the current usr
 */
(function () {
    'use strict';

    function routeAuthService(authService) {
        // region Inner Methods

        /**
         * Checks if the current user can visit the given route
         * @param route
         */
        function canVisitRoute(route){
            var userInfo = authService.getUserInfo(),
                claims = userInfo && userInfo.claims,
                routeClaims = route.claims;

            // route is accessible by everyone
            if(!routeClaims){
                return true;
            }

            // not logged in yet
            if(!claims){
                return false;
            }

            // check route claims against user claims
            for(var routeClaim in routeClaims){
                var routeClaimValues = routeClaims[routeClaim],
                    correspondingClaims = _getUserClaim(claims, routeClaim),

                    commonClaims = correspondingClaims.filter(function(item){
                        return routeClaimValues.indexOf(item.value) !== -1;
                    });

                // element claim and user's claim for don't match
                if(!commonClaims.length){
                    return false;
                }
            }

            return true;
        }

        /**
         * Returns the value for the given claim type
         * @param claims
         * @param claimType
         * @private
         */
        function _getUserClaim(claims, claimType){
            return claims.filter(function(claim){ return claim.type === claimType; });
        }

        // endregion

        // region Public API

        return {
            canVisitRoute: canVisitRoute
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'routeAuthService',
        type: 'factory',
        service: [
            'authService',
            routeAuthService
        ]
    };

    // endregion

})();