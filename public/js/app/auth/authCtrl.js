/**
 * Authentication controller
 */
(function(){
    'use strict';

    function authCtrl(authService){

        // region Viewmodel

        this.username = '';
        this.password = '';

        this.regUsername = '';
        this.regEmail = '';
        this.regPassword = '';
        this.regPasswordRepeat = '';

        this.shouldDisableRegBtn = function regFormValid(invalid){
            if(this.regPasswordRepeat === '' || this.regPassword === ''){
                return true;
            }

            if(this.regPassword === this.regPasswordRepeat){
                return invalid;
            }

            return true;
        };

        this.login = function login(){
            authService.login(this.username, this.password);
        };

        this.signup = function signup(){
            authService.signup(this.regUsername, this.regEmail, this.regPassword);
            authService.login(this.regUsername, this.regPassword);
        };

        // endregion
    }

    module.exports = {
        name: 'authCtrl',
        ctrl: ['authService', authCtrl]
    };
})();