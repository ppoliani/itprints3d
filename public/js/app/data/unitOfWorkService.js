/**
 * unit of work service
 */
(function () {
    'use strict';

    // region Inner Fields

    var _entityManager,
        _repositoriesService,
        _saveOptions;

    // endregion

    // region Inner Methods

    /**
     * Returns the repository of the given type
     * @param type
     */
    function repository(type){
        return _repositoriesService[type + 'Repository'];
    }

    /**
     * Saves all the changes or the set of the given entities
     * * @param {array} selectedEntities -  A list of entities that we want to save (optional)
     */
    function save(selectedEntities){
        var results;

        if (!this.shouldValidate) {
            // temporarily switch off the validation for this entitySet
            _entityManager.validationOptions.validateOnSave = false;
            results = _entityManager.saveChanges(selectedEntities, _saveOptions);
            _entityManager.validationOptions.validateOnSave = true;
        } else {
            results = _entityManager.saveChanges(selectedEntities, _saveOptions);
        }

        return results;
    }

    // endregion

    // region Ctor

    function UnitOfWork(breeze, breezeService, repositoriesService) {
        _entityManager = breezeService.entityManager
        _repositoriesService = repositoriesService;
        _saveOptions = new breeze.SaveOptions({ resourceName: 'saveChanges' });

        this.shouldValidate = false;
    }

    UnitOfWork.prototype = (function(){

        // region public Api

        var publicApi = {
            constructor: UnitOfWork,
            repository: repository,
            save: save
        };

        // endregion

        return publicApi;
    })();

    // endregion

    // region CommonJS

    module.exports = {
        name: 'UnitOfWorkService',
        type: 'service',
        service: [
            'breeze',
            'breezeService',
            'RepositoriesService',
            UnitOfWork]
    };

    // endregion

})();