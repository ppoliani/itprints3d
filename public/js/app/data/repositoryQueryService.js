/**
 * A repository query service that constructs a new query to
 */
(function () {
    'use strict';

    function RepositoryQueryService() {

        // region Inner Methods

        /**
         * Adds a filter expression to this query
         * @param expression
         */
        function filter(expression){
            this._filter = expression;

            return this;
        }

        /**
         * Add the id of the requested resource
         * @param id
         */
        function setResourceId(id){
            this._id = id;

            return this;
        }

        /**
         * Query lookups not the entire entity
         */
        function lookups(){
            this._lookups = true;

            return this;
        }

        /**
         * Add an orderby expression to this query
         * @param expression
         */
        function orderBy(expression){
            this._orderBy = expression;

            return this;
        }

        /**
         * Will eagerly load the given navigation properties
         * @param expression
         */
        function expand(expression){
            this._includeProperties = expression;

            return this;
        }

        /**
         * Skip the given number of items
         * @param pageSkip
         */
        function skip(skip){
            this._skip = skip;

            return this;
        }

        /**
         * Take the given number of items
         * @param pageSize
         * @returns {RepositoryQueryService.take}
         */
        function take(take){
            this._take = take; // pageSkip * pageSize;

            return this;
        }

        /**
         * Indicates that the query should target the local cache
         */
        function local(){
            this._queryLocally = true;

            return this;
        }

        /**
         * Returns the results of this query
         */
        function exec(){
            return this._repository.get({
                filter: this._filter,
                id: this._id,
                lookups: this._lookups,
                orderBy: this._orderBy,
                includeProperties: this._includeProperties,
                skip: this._skip,
                take: this._take,
                queryLocally: this._queryLocally
            });
        }

        // endregion

        // region Ctor

        function RepositoryQuery(repository){
            this._repository = repository;
            this._filter = null;
            this._id = null;
            this._lookups = null;
            this._orderBy = null;
            this._includeProperties = null;
            this._skip = undefined;
            this._take = undefined;
            this._queryLocally = false;
        }

        RepositoryQuery.prototype = (function(){

            // region Public Api

            var publicApi = {
                constructor: RepositoryQuery,
                filter: filter,
                setResourceId: setResourceId,
                lookups: lookups,
                orderBy: orderBy,
                expand: expand,
                skip: skip,
                take: take,
                local: local,
                exec: exec
            };

            // endregion

            return publicApi;
        })();

        // endregion

        // region Public API

        return {
            RepositoryQuery: RepositoryQuery
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'RepositoryQueryService',
        type: 'factory',
        service: [RepositoryQueryService]
    };

    // endregion

})();