/**
 * Aggregates all the models as angular dependencies
 */
(function(){
    'use strict';

    function modelSet(){
        return {
        };
    }

    // region CommonJS

    module.exports = {
        name: 'modelSet',
        type: 'factory',
        service: [
            modelSet
        ]
    };

    // endregion
})();