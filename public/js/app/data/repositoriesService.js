/**
 * A collection of all the repositories in the application
 */
(function () {
    'use strict';

    function RepositoriesService(RepositoryService) {

        // region Inner Fields

        var Repository = RepositoryService.Repository;

        // endregion

        // region Public API

        return {
            PrinterRepository: new Repository('Printer', 'printers', 'printers/lookups'),
            PrinterSuggestedRepository: new Repository('Printer', 'printers', 'printers/suggested/lookups')
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'RepositoriesService',
        type: 'factory',
        service: ['RepositoryService', RepositoriesService]
    };

    // endregion

})();