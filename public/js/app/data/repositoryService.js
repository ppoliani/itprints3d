/**
 * The repository service
 */
(function () {
    'use strict';

    function RepositoryService(breeze, logger, RepositoryQueryService, breezeService) {

        // region Inner Fields

        var entityManager = breezeService.entityManager;

        // endregion

        // region Inner Methods

        /**
         * Returns the entity with the given id. Checks the cache first
         * @param id
         */
        function findById(id){
            return entityManager.fetchEntityByKey(this.entityName, id, true);
        }

        /**
         * Inserts the given entity into the cache
         * @param entity
         */
        function attach(entity){
            return entityManager.createEntity(this.entityName, entity);
        }

        /**
         * Updates the given entity
         * @param entity
         */
        function update(entity){
            // This will overwrite the entity in the cache with the same id
            return entityManager.createEntity(this.entityName, entity, null, breeze.MergeStrategy.PreserveChanges);
        }

        /**
         * Deletes the given entity
         * @param entity
         */
        function deleteEntity(entity){
            entity.entityAspect.setDeleted();
        }

        /**
         * Return a new instance of the repository query class
         */
        function query(){
            return new RepositoryQueryService.RepositoryQuery(this);
        }

        /**
         * Will execute and return the query with the given options
         * @param opts
         */
        function get(opts){
            var q = breeze.EntityQuery;

            if(opts.lookups){
                if(!this.lookups) throw new Error('Lookups endpoint not specified for this resource: ');
                q = q.from(this.lookups);
            }
            else {
                q = q.from(this.resourceName);
            }

            if(opts.queryLocally){
                q = q.toType(this.entityName);
            }

            if(opts.filter){
                q = q.where(breeze.Predicate.create.apply(breeze.Predicate, opts.filter));
            }

            if(opts.id){
                q = q.setResourceId(opts.id);
            }

            if(opts.orderBy){
                q =  q.orderBy(opts.orderBy);
            }

            if(opts.includeProperties){
                q = q.expand(opts.includeProperties);
            }

            if(opts.skip){
                q = q.skip(opts.skip)
            }

            if(opts.take){
                q = q.take(opts.take);
            }

            if(opts.queryLocally){
                return entityManager.executeQueryLocally(q);
            }

            else {
               return _executeQuery.call(this, q);
            }
        }

        /**
         * Will execute the given query
         * @param q
         * @private
         */
        function _executeQuery(q){
            var promise = entityManager.executeQuery(q);

            promise
                .then(_querySucceeded.bind(this))
                .catch(_queryFailed.bind(this));

            return promise;
        }

        /**
         * Success callback
         * @private
         */
        function _querySucceeded(){
            logger.success('Data for [' + this.entityName + '] was loaded');
        }

        /**
         * Fail callback
         * @param error
         * @private
         */
        function _queryFailed(error){
            logger.error('Error retrieving data for the entity type [' + this.entityName + '] ' + error.message);
        }

        // endregion

        // region Ctor

        function Repository(entityName, resourceName, lookups){
            this.entityName = entityName;
            this.resourceName = resourceName;
            this.lookups = lookups;
        }

        Repository.prototype = (function(){

            // region Public Api

            var publicApi = {
                constructor: Repository,
                findById: findById,
                attach: attach,
                update: update,
                deleteEntity: deleteEntity,
                query: query,
                get: get
            };

            // endregion

            return publicApi;
        })();

        // endregion

        // region Public API

        return {
            Repository: Repository
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'RepositoryService',
        type: 'factory',
        service: [
            'breeze',
            'logger',
            'RepositoryQueryService',
            'breezeService',
            RepositoryService]
    };

    // endregion

})();