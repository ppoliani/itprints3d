/**
 * Ctlr for the ask product question form
 */
 (function(){
    'use strict';

    function ProductQuestionCtrl($scope){
        // region Inner Fields
        // endregion

        // region Inner Methods

        // endregion

        // region Viewmodel

        this.product = {
            productName: $scope.ngDialogData.productName,
            shortDescription: $scope.ngDialogData.shortDescription,
            productImg: $scope.ngDialogData.productImg
        };

        this.imageUrl = this.product.productImg;

        this.form = {};

        $scope.askQuestion = function askQuestion($invalid){
            throw 'Not Implemented';
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'ProductQuestionCtrl',
        ctrl: [
            '$scope',
            'imgUriBuilderService',
            ProductQuestionCtrl
        ]
    };

    // endregion

 })();
