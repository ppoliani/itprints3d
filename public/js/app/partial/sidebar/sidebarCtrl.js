/**
 * Sidebar controller
 */
 (function(){
    'use strict';

    function sidebarCtrl(){
        // region Inner Methods

        // endregion

        // region Viewmodel

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'sidebarCtrl',
        ctrl: [sidebarCtrl]
    };

    // endregion

 })();
