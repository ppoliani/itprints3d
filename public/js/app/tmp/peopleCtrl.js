/**
 * temporary rest
 */
(function () {
    'use strict';

    var PeopleCtrl = function Ctrl($scope, unitOfWork, logger) {

        // region Inner Fields

        var personRepository = unitOfWork.repository('person'),
            currentAction = 'add';

        // endregion

        // region Inner Methods

        var _loadData = function _loadData(){
            personRepository.query()
                .orderBy('name')
                .exec()
                .then(function(people){
                    $scope.people = people.results;
                });
        };

        // endregion

        // region Viewmodel

        $scope.person;
        $scope.actionBtnTitle = 'Add';

        $scope.addEditPerson = function addPerson() {
            if(currentAction === 'add'){
                personRepository.attach($scope.person);
            }
            else {
                personRepository.update($scope.person);
            }
            unitOfWork.save()
                .then(function(){
                    currentAction = 'add';
                    $scope.actionBtnTitle = 'add';
                })
                .catch(function(message){
                    logger.error('An error ocurred: ' + message);
                });
        };

        $scope.removePerson = function removePerson(personToRemove){
            logger.confirm('Are you sure you want to delete this entity?')
                .then(function(answer){
                    if(answer){
                        personRepository.deleteEntity(personToRemove);
                        unitOfWork.save()
                            .then(function(){
                                logger.success('The entity ' + personToRemove.id + ' was removed');
                            })
                            .catch(function(message){
                                logger.error('The entity ' + personToRemove.id + ' could not be removed: ' + message);
                            });
                    }
                });
        };

        $scope.editPerson = function editPerson(personToEdit){
            $scope.person = personToEdit;
            currentAction = 'update';
            $scope.actionBtnTitle = 'update';
        };

        // endregion

        _loadData();
    };

    // region CommonJS

    module.exports = {
        name: 'peopleCtrl',
        ctrl: [
            '$scope',
            'UnitOfWorkService',
            'logger',
            PeopleCtrl
        ]
    };

    // endregion

})();