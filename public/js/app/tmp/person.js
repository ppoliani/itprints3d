/**
 * The Person model
 */
(function () {
    'use strict';

    var personModel = function(){

        // region Inner Methods


        // endregion

        // region Ctor

        var Person = function Person() {
            this.id = undefined;
            this.name = undefined;
            this.hobbies = null;
            this.address = null;
        };

        Person.prototype = (function(){

            // region Public Api

            return {
                constructor: Person
            };

            // endregion

        })();

        return Person;

        // endregion

    };

    // region CommonJS

    module.exports = {
        name: 'Person',
        ctor: [personModel]
    };

    // endregion

})();