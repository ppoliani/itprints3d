/**
 * Defers the loading of critical resources
 */
 (function(){
    'use strict';

    function criticalResourceDirective(){

        // region Inner Methods

        function link(scope){
            var script = document.createElement("script"),
                css = document.createElement("link");

            // if only css is loaded
            if(!scope.js && scope.css){
                css.onload = function(){
                    _onload(scope);
                };
            }
            else {
                script.onload = function(){
                    _onload(scope);
                };
            }

            if(scope.css) {
                css.rel = "stylesheet";
                css.type = "text/css";
                css.href = scope.css;
                document.getElementsByTagName('head')[0].appendChild(css);
            }

            if(scope.js) {
                script.src = scope.js;
                document.body.appendChild(script);
            }
        }

        function _onload(scope){
            _onload.invoked = true;

            scope.loaded = true;
            scope.$apply();
        }

        // endregion

        return {
            restrict: 'AE',
            scope: {
                loaded: '=?',
                js: '@?',
                css: '@?'
            },
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'criticalResource',
        directive: [criticalResourceDirective]
    };

    // endregion

 })();