/**
 * Generic component for product page
 */
 (function(){
    'use strict';

    function productPageDirective(){

        function productPageCtrl($stateParams, logger, uof, IMAGE_URL, IMAGE_PROP_SUFFIX){
            // region Inner Fields

            var productRepository = uof.repository(this.productType);

            // endregion

            // region Inner Methods

            function _loadData(){
                this.productPromise = productRepository.query()
                    .setResourceId($stateParams.id)
                    .exec()
                    .then(function(entity){
                        this.product = entity.results[0];
                        this.images = this.product[_toLowerCaseFirstLetter(this.productType) + IMAGE_PROP_SUFFIX].map(function(image){ return  IMAGE_URL + image + '?width=100&height=100'; });
                    }.bind(this))
                    .catch(function(err){
                        logger.error('Error while reading the ' + this.product + ' with the id: ' + $stateParams.id);
                    }.bind(this));
            }

            function _toLowerCaseFirstLetter(str){
                return str.charAt(0).toLowerCase() + str.slice(1);
            }

            // endregion

            // region Viewmodel

            this.printer = null;
            this.images = [];

            // endregion

            _loadData.call(this);
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/productPage/productPage.html',
            scope: {
                productType: '@'
            },
            controller: [
                '$stateParams',
                'logger',
                'UnitOfWorkService',
                'IMAGE_URL',
                'IMAGE_PROP_SUFFIX',
                productPageCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'productPage',
        directive: [productPageDirective]
    };

    // endregion

 })();