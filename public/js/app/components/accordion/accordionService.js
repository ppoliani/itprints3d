/**
 * A service to communicate accordion with the outer world
 */
 (function(){
    'use strict';

    function accordionService(){

        // region Inner Fields

        var _accordionContainerApi;

        // endregion

        // region Inner Methods

        function registerAccordionContainer(accordionContainerApi){
            _accordionContainerApi = accordionContainerApi;
        }

        function openNext(){
            _accordionContainerApi.openNext();
        }

        // endregion

        // region Public API

        return {
            registerAccordionContainer: registerAccordionContainer,
            openNext: openNext
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'accordionService',
        type: 'factory',
        service: [accordionService]
    };

    // endregion

 })();