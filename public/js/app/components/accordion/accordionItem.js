/**
 * Accordion item directive
 */
 (function(){
    'use strict';

    function accordionItemDirective(){

        function link(scope, element, attrs, ctrl, transcludeFn){
            var index = ctrl.addAccordionItem(scope); 

            scope.shouldDisplay = false;

            transcludeFn(function(clone, transcudedScope){
                if(scope.unsafeHtml){
                    scope.contentPromise.then(function(){
                        element.find('div').append($parse(scope.unsafeHtml)(transcudedScope));
                    });
                }
            });

            scope.select = function select(){
                ctrl.itemSelected(index);
            };
        }

        return {
            restrict: 'AE',
            transclude: true,
            require: '^accordion',
            templateUrl: '/js/app/components/accordion/accordionItem.html',
            scope: {
                header: '@',
                unsafeHtml: '@?',
                contentPromise: '='
            },
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'accordionItem',
        directive: [accordionItemDirective]
    };

    // endregion

 })();