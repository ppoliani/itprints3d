/**
 * Accordion container
 */
 (function(){
    'use strict';

    function accordionDirective(){

        function link(scope){
            // select first item by default
            scope.vm.itemSelected(parseInt(scope.vm.selectedItem));
        }

        function accordionCtrl(accordionService){

            // region Inner Fields

            var _accordionItems = [],
                _itemsOpened = [true],
                _currentIndex = 0;

            // endregion

            // region Inner Methods

            function _canOpenItem(index){
                return _itemsOpened[index] || index === parseInt(this.selectedItem);
            }

            // endregion

            // region Viewmodel

            this.addAccordionItem = function addAccordionItem(scope){
                // 0-based indexing
                return _accordionItems.push(scope) - 1;
            };

            this.itemSelected = function itemSelected(selectedIndex){
                if(this.sequential){
                    if(_canOpenItem.call(this, selectedIndex)){
                        // close the prev item
                        if(_currentIndex !== selectedIndex) {
                            _accordionItems[_currentIndex].shouldDisplay = false;
                        }

                        _accordionItems[selectedIndex].shouldDisplay = !_accordionItems[selectedIndex].shouldDisplay;

                        _currentIndex = selectedIndex;
                        _itemsOpened[_currentIndex] = true;
                    }
                }
                else {
                    throw 'Not Implemented';
                }
            };

            /**
             * Opens the next accordion item
             */
            this.openNext = function openNext(){
                _itemsOpened[_currentIndex + 1] = true;
                this.itemSelected(_currentIndex + 1);
            };

            // endregion

            // region Setup

            accordionService.registerAccordionContainer({
                openNext: this.openNext.bind(this)
            });

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/accordion/accordionContainer.html',
            transclude: true,
            scope: {
                sequential: '=?',
                selectedItem: '@?'
            },
            controller: [
                'accordionService',
                accordionCtrl
            ],
            controllerAs: 'vm',
            bindToController: true,
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'accordion',
        directive: [accordionDirective]
    };

    // endregion

 })();