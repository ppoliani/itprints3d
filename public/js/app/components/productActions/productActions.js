/**
 * Component that holds various actions that one can perform on a product
 */
 (function(){
    'use strict';

    function productActionsDirective(){
        function productActionsCtrl(ngDialog, shoppingCartService){
            // region Viewmodel

            this.cartQuantity = 1;

            /**
             * Adds the current prouct to the shopping cart
             */
            this.addToCart = function addToCart(){
                shoppingCartService.add(this.product, this.productType, this.cartQuantity);
            };

            /**
             * Displays a modal that user can use to ask a question about this product
             */
            this.askQuestion = function askQuestion(){
                ngDialog.open({
                    template: '/js/app/partial/productQuestion/productQuestion.html',
                    controller: 'ProductQuestionCtrl as vm',
                    data: {
                        productName: this.product.productName,
                        shortDescription: this.product.shortDescription,
                        productImg: this.productImage
                    }
                });
            };

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/productActions/productActions.html',
            scope: {
                product: '=',
                productImage: '=',
                productType: '@'
            },
            controller: [
                'ngDialog',
                'shoppingCartService',
                productActionsCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'productActions',
        directive: [productActionsDirective]
    };

    // endregion

 })();