/**
 * Image magnifier
 */
 (function(){
    'use strict';

    function imageMagnifierDirective(Event, Magnifier){

        // region Inner Methods

        function link(scope, element, attrs){
            var options = {
                    thumb: '#' + attrs.id,
                    large: scope.largeImage,
                    mode: 'inside',
                    zoom: scope.zoom || 2,
                    zoomable: true
                },

                magnifier = new Magnifier(new Event());

            // attach the first time it's invokeds
            scope.$watch('image', function on_imageChange(){
                if(!on_imageChange.invoked){
                    magnifier.attach(options);

                    on_imageChange.invoked = true;
                }
            });

            scope.$watch('largeImage', function(oldVal, newVal){
                if(oldVal !== newVal){
                    options.large = scope.largeImage;
                    magnifier.set(options, true);
                }
            });
        }

        // endregion

        return {
            restrict: 'AE',
            scope: {
                image: '@',
                largeImage: '@',
                mode: '@?',
                zoom: '@?'
            },
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'imageMagnifier',
        directive: [
            'Event',
            'Magnifier',
            imageMagnifierDirective
        ]
    };

    // endregion

 })();