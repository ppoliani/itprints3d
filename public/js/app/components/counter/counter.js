/**
 * Counter component
 */
 (function(){
    'use strict';

    function counterDirective(){

        function counterCtrl(){
            // region Viewmodel

            this.increment = function increment(){
                this.value += 1;
            };

            this.decrement = function decrement(){
                this.value = Math.max(0, --this.value);
            };

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/counter/counter.html',
            scope: {
                value: '='
            },
            controller: [counterCtrl],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'counter',
        directive: [counterDirective]
    };

    // endregion

 })();