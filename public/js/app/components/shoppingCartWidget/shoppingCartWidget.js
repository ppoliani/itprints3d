/**
 * A shopping cart widget that show the number of items
 * that are currently in the shopping cart
 */
 (function(){
    'use strict';

    function shoppingCartWidgetDirective(){

        function shoppingCartWidgetCtrl(shoppingCartService, pubsub){

            // region Inner Methods

            function _init(){
                _getProductsCount.call(this)
                    .then(_getTotalPrice.bind(this));
            }

            function _getProductsCount(){
                return shoppingCartService.getProductsCount()
                    .then(function(count){
                        this.itemsCount = count;
                    }.bind(this));
            }

            function _getTotalPrice(){
                shoppingCartService.getProductsTotalPrice()
                    .then(function(totalPrice){
                        this.subtotal = totalPrice;
                    }.bind(this));
            }

            // endregion

            // region Events

            pubsub.on('shoppingCart:update', _init.bind(this));

            // endregion

            _init.call(this);
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/shoppingCartWidget/shoppingCartWidget.html',
            scope: {},
            controller: [
                'shoppingCartService',
                'pubSubService',
                shoppingCartWidgetCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'shoppingCartWidget',
        directive: [shoppingCartWidgetDirective]
    };

    // endregion

 })();