/**
 * A directive that displays validation errors for an input element
 */
 (function(){
    'use strict';

    function validationErrorDirective(){

        // region Const

        var MESSAGES = {
            required: 'This field is required',
            minlength: 'Too short input',
            maxlength: 'Too long input',
            number: 'The input value needs to be a numeric value',
            email: 'Please type a correct email format'
        };

        // endregion

        // region Inner Methods

        /**
         * Directive link funciton
         */
        function linkFn(scope, element, attrs){
            var validationRules = _getValidationRules(attrs),
                inputName = attrs.name,
                form = scope[attrs.validationError],
                $errors = form[inputName].$error,
                placeholder = attrs.placeholder,
                validationErrors, errorMessages;


            element.addClass('input--validation');

            element.on('blur keyup change', function(){
                validationErrors = _getValidationErrors($errors, validationRules);
                errorMessages = validationErrors.map(function(validationError){ return MESSAGES[validationError]; })

                if(validationErrors.length && (form.$submitted || form[inputName].$touched)){
                    element.addClass('input--error');
                    element.attr('placeholder', errorMessages);

                    scope.$apply();
                }
                else {
                    element.removeClass('input--error');
                    element.attr('placeholder', placeholder);
                }
            });
        }

        /**
         * Returns the validation rules that are applied to the element in question
         * @param attrs
         * @private
         */
        function _getValidationRules(attrs){
            var rules = [];

            if(attrs.required){
                rules.push('required');
            }
            if(attrs.type === 'number'){
                rules.push('number');
            }
            if(attrs.min){
                rules.push('min');
            }

            if(attrs.ngMinLength){
                rules.push('minlength');
            }

            if(attrs.max){
                rules.push('max');
            }

            if(attrs.ngMaxlength){
                rules.push('maxlength');
            }

            if(attrs.type === 'email'){
                rules.push('email');
            }

            return rules;
        }

        /**
         * Returns validation error if they exist
         * @param errors
         * @private
         */
        function _getValidationErrors($error, rules){
            return rules.reduce(function(prev, current){
                if($error[current]){
                    prev.push(current);
                }

                return prev;
            }, []);
        }

        // endregion

        return {
            restrict: 'AE',
            scope: true,
            link: linkFn
        };
    }

    // region CommonJS

    module.exports = {
        name: 'validationError',
        directive: [validationErrorDirective]
    };

    // endregion

 })();