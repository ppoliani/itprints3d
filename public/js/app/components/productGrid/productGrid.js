/**
 * A generic product grid
 */
 (function(){
    'use strict';

    function productGridDirective(){
        function productGridCtrl($scope, $sce, uof, imgUriBuilder, shoppingCart, ORDER_BY_OPTIONS){
            // region Inner Fields

            var repository =  uof.repository(this.repository);

            // endregion

            // region Inner Methods

            function _load(){
                var q = repository.query()
                        .lookups()
                        .skip((this.currentPage - 1) * Number(this.itemsPerPage))
                        .take(Number(this.itemsPerPage));

                    if(this.orderBy){
                        q = q.orderBy(this.orderBy);
                    }

                    q.exec()
                        .then(function(entities){
                            this.products = entities.results;
                            this.totalItems = entities.httpResponse.data.total;
                            _updateResultsCountLabel.call(this);
                        }.bind(this));
            }

            function _updateResultsCountLabel(){
                var upperLimit = this.totalItems > this.itemsPerPage ? this.itemsPerPage : this.totalItems;

                this.resultsLabel = 'Results ' + this.currentPage + '-' + upperLimit + ' of ' + this.totalItems;
            }

            // endregion

            // region Viewmodel

            this.Modes = {
                GRID: 0,
                LIST: 1
            };

            this.mode = this.Modes.GRID;

            this.ORDER_BY_OPTIONS = ORDER_BY_OPTIONS;
            this.orderBy = "";

            this.itemsPerPage = this.itemsPerPage || 5;

            this.pagination = this.gridPagination;
            this.currentPage = 1;
            this.totalItems = undefined;

            /**
             * Returns the image url of the given product
             * @param product
             * @returns {string}
             */
            this.getProductUrl = function getProductUrl(product){
                return imgUriBuilder.buildImageUri(product.printer_ImageID[0], 300, 300);
            };

            /**
             * Returns the short description of the given product
             */
            this.getShortDescription = function getShortDescription(product){
                // ToDo: return the short description when it's available
                return $sce.trustAsHtml(product.productName);
            };

            /**
             * Displays the products as a list view
             */
            this.changeView = function changeView(mode){
                this.mode = mode;
            };

            /**
             * Adds the given product to the shopping cart
             * @param product
             */
            this.addToShoppingCart = function addToShoppingCart(product){
                shoppingCart.add(product, repository.entityName, 1);
            };

            $scope.$watch(function(){ return this.orderBy; }.bind(this), _load.bind(this));
            $scope.$watch(function(){ return this.itemsPerPage; }.bind(this), _load.bind(this));

            // endregion

            _load.call(this);
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/productGrid/productGrid.html',
            scope: {
                repository: '@',
                productUrl: '@',
                gridPagination: '=',
                itemsPerPage: '@',
                showControls: '='
            },
            controller: [
                '$scope',
                '$sce',
                'UnitOfWorkService',
                'imgUriBuilderService',
                'shoppingCartService',
                'ORDER_BY_OPTIONS',
                productGridCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'productGrid',
        directive: [productGridDirective]
    };

    // endregion

 })();