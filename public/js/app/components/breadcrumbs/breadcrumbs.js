/**
 * Breadcrumbs directive that utilizes the foundation's breadcrumb component
 */
 (function(){
    'use strict';

    function breadcrumbsDirective($state, pubsub){

        // region Inner Methods

        function compile(element){
            var breadcrumbsEl = element.find('ul');

            pubsub.on('breadcrumbs:update', function on_breadcrumbs_update(items){
                if(!items.length){
                    breadcrumbsEl.addClass('hidden');
                }
                else {
                    breadcrumbsEl.removeClass('hidden');

                    var html = '<li><a href="'+ $state.href('home') +'">Home</a></li>',
                        lastIndex = items.length - 1;

                    items.forEach(function(item, index){
                        if(index === lastIndex){
                            html += '<li class="current"><a>' + item.name + '</a></li>';
                        }
                        else {
                            html += '<li><a href="'+ $state.href(item.state) +'">' + item.name + '</a></li>';
                        }
                    });

                    breadcrumbsEl.html(html);
                }
            });
        }

        // endregion

        return {
            restrict: 'AE',
            template: '<ul class="breadcrumbs"></ul>',
            compile: compile
        };
    }

    // region CommonJS

    module.exports = {
        name: 'breadcrumbs',
        directive: [
            '$state',
            'pubSubService',
            breadcrumbsDirective
        ]
    };

    // endregion

 })();