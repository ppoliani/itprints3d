/**
 * A rating component
 */
 (function(){
    'use strict';

    function ratingDirective(){

        function link(scope){
            var STAR_EMPTY = "icon--star",
                STAR_HALF = "icon--star2",
                STAR_FULL = "icon--star3",

                normalizedScore = scope.score / 20, // 1 star --> 20%
                halfStarExists = normalizedScore % 1 >= 0.5,
                numOfFullStars = Math.floor(normalizedScore);

            scope.title = normalizedScore + ' out of 5';
            scope.stars = _.map(_.range(5), function () { return STAR_EMPTY; });

            for (var i = 0; i < numOfFullStars; i++) {
                scope.stars[i] = STAR_FULL;
            }

            if (halfStarExists) {
                scope.stars[numOfFullStars] = STAR_HALF;
            }
        }

        return {
            restrict: 'AE',
            scope: {
                score: '=',
                count: '='
            },
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'rating',
        directive: [ratingDirective]
    };

    // endregion

 })();