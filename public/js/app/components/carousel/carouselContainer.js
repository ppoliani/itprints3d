/**
 * carousel container
 */
 (function(){
    'use strict';

    function carouselDirective(){
        var THUMB_MARGIN = 10,
            CONTROLS_WIDTH = 40;

        function link(scope, element){
            var $containerWidth = element.parent()[0].offsetWidth;
            scope.vm.thumbWidth = ($containerWidth - CONTROLS_WIDTH - (scope.vm.viewableThumbs * THUMB_MARGIN)) / scope.vm.viewableThumbs;
        }

        function carouselCtrl(){

            // region Inner Fields

            var _carouselItems = [],
                _prevSelectedSlide;

            // endregion

            // region Inner Methods

            function _shouldMoveToTheRight(){
                return this.currentPos + ((this.thumbWidth + THUMB_MARGIN) * this.viewableThumbs) < this.sliderContainerWidth;
            }

            function _selectCarouselItem(el){
                _prevSelectedSlide && _prevSelectedSlide.removeClass('selected-slide');
                el.addClass('selected-slide');
                _prevSelectedSlide = el;
            }

            // endregion

            // region Viewmodel

            this.currentPos = 0;

            this.addCarouselItem = function(element){
                _carouselItems.push(element);
                _selectCarouselItem(_carouselItems[0]);

                this.sliderContainerWidth = (_carouselItems.length * THUMB_MARGIN) + ( _carouselItems.length * this.thumbWidth);
            };

            this.slideSelected = function slideSelected(el, slideIndex){
                _selectCarouselItem(el);
                this.onSlideSelected()(slideIndex);
            };

            this.moveLeft = function moveLeft(){
                if (this.currentPos > 0) {
                    this.currentPos  -= this.thumbWidth + THUMB_MARGIN;
                }
            };

            this.moveRight = function moveRight(){
                if (_shouldMoveToTheRight.call(this)) {
                    this.currentPos  += this.thumbWidth + THUMB_MARGIN;
                }
            };

            // endregion

        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/carousel/carouselContainer.html',
            transclude: true,
            scope: {
                onSlideSelected: '&',
                viewableThumbs: '@'
            },
            link: link,
            controller: [carouselCtrl],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'carousel',
        directive: [carouselDirective]
    };

    // endregion

 })();