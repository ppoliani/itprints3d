/**
 * Carousel item
 */
 (function(){
    'use strict';

    function carouselItemDirective(){

        function link(scope, element, attrs, ctrl, transcludeFn){
            ctrl.addCarouselItem(element);

            transcludeFn(function(clone, transcudedScope){
                if(scope.unsafeHtml){
                    scope.contentPromise.then(function(){
                        element.find('div').html($parse(scope.unsafeHtml)(transcudedScope));
                    });
                }
            });

            scope.selectSlide = function selectSlide(){
                ctrl.slideSelected(element, scope.slideIndex);
            };

            scope.thumbWidth = ctrl.thumbWidth;

            if(scope.selectable){
                element.addClass('carousel__slide--selectable');
            }
        }

        return {
            restrict: 'AE',
            transclude: true,
            require: '^carousel',
            templateUrl: '/js/app/components/carousel/carouselItem.html',
            scope: {
                unsafeHtml: '@?',
                slideIndex: '=?',
                selectable: '=?',
                contentPromise: '=?'
            },
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'carouselItem',
        directive: [carouselItemDirective]
    };

    // endregion

 })();