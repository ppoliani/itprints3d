/**
 * A directive that will be populated by the back-end
 * with the preloaded data
 */
(function(){
    'use strict';

    function preloadResourceDirective($rootScope) {

        // region Inner Methods

            function link(scope, element, attrs){
                $rootScope.breezeMetadata = attrs.resourceContent;
                element.remove();
            }

        // endregion

        return {
            restrict: 'AE',
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'preloadResource',
        directive: ['$rootScope',preloadResourceDirective]
    };

    // endregion
})();