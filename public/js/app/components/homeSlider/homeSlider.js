/**
 * Home slider component. Depends on swiper lib
 */
 (function(){
    'use strict';

    function homeSliderDirective(){

        function homeSliderCtrl($scope){
                var _swiper;

                this.dynamicScriptLoaded = false;

                $scope.$watch(function(){ return this.dynamicScriptLoaded; }.bind(this), function(loaded){
                    if(loaded) {
                        _swiper = new Swiper('.swiper-container', {
                            pagination: '.pagination',
                            autoplay: 5000,
                            mode: 'horizontal',
                            loop: true,
                            grabCursor: true,
                            paginationClickable: true
                        });
                    }
                });


                this.moveLeft = function moveLeft(){
                    _swiper.swipePrev();
                };

                this.moveRight = function moveRight(){
                    _swiper.swipeNext();
                };
            }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/homeSlider/homeSlider.html',
            controller: [
                '$scope',
                homeSliderCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'homeSlider',
        directive: [homeSliderDirective]
    };

    // endregion

 })();