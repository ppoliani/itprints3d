/**
 * A directive that triggers the breeze initialization process, this is used becuase we want the rootScope to be populated with the metadata before the init process starts
 */
 (function(){
    'use strict';

    function breezeInitDirective(breezeService){

        // region Inner Methods

        function link(scope, element){
            breezeService.init();
            element.remove();
        }

        // endregion

        return {
            restrict: 'AE',
            link: link
        };
    }

    // region CommonJS

    module.exports = {
        name: 'breezeInit',
        directive: ['breezeService', breezeInitDirective]
    };

    // endregion

 })();