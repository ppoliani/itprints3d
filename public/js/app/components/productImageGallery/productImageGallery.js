/**
 * Image gallery that holds the images for a product
 */
 (function(){
    'use strict';
    
    function productImageGalleryDirective(){
    
        function productImageGalleryCtrl($scope){

            // region Inner Methods

            function _buildImgUri(image, width, height){
                if(image){
                    return image.split('?')[0] + '?width=' + width + '&height=' + height;
                }
            }

            // endregion

            // region Viewmodel

            this.selectImage = function selectImage(imgIndex){
                var _selectedImage = this.images[imgIndex].split('?')[0];

                this.selectedImage = _buildImgUri(_selectedImage, 300, 300);
                this.selectedLargeImage = _buildImgUri(_selectedImage, 600, 600);
            }.bind(this);

            $scope.$watch(function(){ return this.images; }.bind(this), function(){
                this.selectedImage = _buildImgUri(this.images[0], 300, 300);
                this.selectedLargeImage = _buildImgUri(this.images[0], 600, 600);
            }.bind(this));

            $scope.$watch(function(){ return this.selectedImage; }.bind(this), function(){
                if(this.selectedImage){
                    this.selectedImage = _buildImgUri(this.selectedImage.split('?')[0], 300, 300);
                    this.selectedLargeImage = _buildImgUri(this.selectedImage.split('?')[0], 600, 600);
                }
            }.bind(this));

            // endregion
        }
        
        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/productImageGallery/productImageGallery.html',
            scope: {
                images: '='
            },
            controller: [
                '$scope',
                productImageGalleryCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }
    
    // region CommonJS
    
    module.exports = {
        name: 'productImageGallery',
        directive: [productImageGalleryDirective]
    };
    
    // endregion
    
 })();