/**
 * The shopping cart grid directive
 */
 (function(){
    'use strict';

    function shoppingCartDirective(){

        function shoppingCartCtrl($state, shoppingCartService, imgUriBuilder, logger){

            // region Inner Fields

            var shoppingCart;

            // endregion

            // region Setup

            shoppingCartService.getShoppingCart()
                .then(on_response.bind(this))
                .catch(function(){
                    logger.error('Unable to load the shopping cart contents');
                });

            // endregion

            // region Viewmodel

            this.getProductImageUrl = function getProductImageUrl(product){
                return imgUriBuilder.buildProductImageUri(product.productID, 75, 75, product.category);
            };

            this.getProductUrl = function getProductUrl(product){
                var productPartialUrl;

                switch(product.category){
                    case 'Printer':
                        productPartialUrl = 'printers/';
                        break
                }

                return productPartialUrl + product.productID;
            };

            this.getSubtotal = function getSubtotal(product){
                return product.price * product.quantity;
            };

            this.deleteProduct = function deleteProduct(product){
                shoppingCartService.remove(product)
                    .then(on_response.bind(this));
            };

            this.update = function update(){
                shoppingCartService.update()
                    .then(on_response.bind(this));
            };

            this.continueShopping = function update(){
                return $state.go('home');
            };

            this.clear = function clear(){
                this.products = [];
                shoppingCartService.clear();
            };

            this.checkout = function checkout(){
                $state.go('checkout');
            };

            // endregion

            // region Events

            function on_response(_shoppingCart_){
                shoppingCart = _shoppingCart_;
                this.products = shoppingCart.cartProducts || [];
            }

            // endregion
        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/shoppingCart/shoppingCart.html',
            controller: [
                '$state',
                'shoppingCartService',
                'imgUriBuilderService',
                'logger',
                shoppingCartCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'shoppingCart',
        directive: [shoppingCartDirective]
    };

    // endregion

 })();