/**
 * Checkout page
 */
 (function(){
    'use strict';

    function checkoutPageDirective(){

        function checkoutPageCtrl(accordionService, authService, ecommerceDataService, shoppingCartService, pubSub, logger){

            // region Setup

            /**
             * Sends the start checkout request
             */
            function _startCheckoutProcess(){
                shoppingCartService.getShoppingCart()
                    .then(function(shoppingCart){
                        this.products = shoppingCart.cartProducts;
                        _shoppingCartId = shoppingCart.id;

                        ecommerceDataService.checkout(_shoppingCartId)
                            .catch(function(){
                                logger.error('Could not start the checkout process. Please reload the page and try again.');
                            });
                    }.bind(this));
            }

            _startCheckoutProcess.call(this);

            // endregion

            // region Inner Fields

            var _shoppingCartId;

            // endregion

            // region Inner Methods

            /**
             * Returns the cars start date year options
             * @private
             */
            function _getStartDateYears(){
                var currentYear = (new Date()).getFullYear();

                return [null, null, null, null, null].reduce(function(prev){
                    prev.unshift(currentYear--);
                    return prev;
                }, []);
            }

            /**
             * Returns the cars expiry date year options
             * @private
             */
            function _getExpiryDateYears(){
                var currentYear = (new Date()).getFullYear();

                return [null, null, null, null, null, null, null, null, null, null, null, null].reduce(function(prev){
                    prev.push(currentYear++);
                    return prev;
                }, []);
            }

            function _copyBillingInfoToShippingInfo(){
                var billingInfo = this.orderDetails.billingInfo;

                this.orderDetails.shippingInfo = {
                    title: billingInfo.title,
                    firstName: billingInfo.firstName,
                    lastName: billingInfo.lastName,
                    country: billingInfo.country,
                    company: billingInfo.company,
                    houseNumber: billingInfo.houseNumber,
                    addressLine: billingInfo.addressLine,
                    addressLine2: billingInfo.addressLine2,
                    city: billingInfo.city,
                    county: billingInfo.county,
                    postcode: billingInfo.postcode,
                    phoneNumber: billingInfo.phoneNumber
                };
            }

            // endregion

            // region Viewmodel

            this.titleOptions = ['Mr', 'Mrs', 'Miss', 'Ms'];
            this.genderOptions = ['Male', 'Female', 'Rather not say'];
            this.cardTypes = ['American Express', 'Delta', 'Mastercard', 'Solo', 'Maestro', 'Visa', 'Visa Electron'];
            this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'October', 'November', 'December'];
            this.starDateYears = _getStartDateYears();
            this.expiryDateYears = _getExpiryDateYears();
            this.products = [];

            this.orderDetails = {
                deliverTo: 'billingAddress',
                billingInfo: {},
                shippingInfo: {},
                paymentInfo: {
                    startDate: {},
                    expiryDate: {}
                }
            };

            this.showCheckouMethod = function showCheckouMethod(){
                return !authService.isLoggedIn();
            };

            /**
             * Checks if the checkout method has been determined
             */
            this.checkoutMethodDetermined = function checkoutMethodDetermined(emailValid){
                return emailValid || authService.isLoggedIn();
            };

            this.moveToNextStep = function moveToNextStep($valid){
                if($valid && (authService.isLoggedIn() || this.orderDetails.email)){
                    accordionService.openNext();
                }
            };

            this.moveToShippingInformation = function moveToShippingInformation($valid){
                if($valid) {
                    if(this.orderDetails.deliverTo === 'billingAddress'){
                        _copyBillingInfoToShippingInfo.call(this);

                        // open the next two oitems
                        accordionService.openNext();
                        accordionService.openNext();
                    }
                }
            };

            this.getSubtotal = function getSubtotal(product){
                return product.price * product.quantity;
            };

            this.getShoppingCartSubtotal = function getShoppingCartSubtotal(){
                return this.products.reduce(function(curr, product){
                    return curr + product.price * product.quantity;
                }, 0);
            };

            this.getShippingCost = function getShippingCost(){
                // ToDo: request the cost from back end
                return 5;
            };

            this.placeOrder = function placeOrder(){
                this.orderDetails.cartID = _shoppingCartId;

                ecommerceDataService.placeOrder(this.orderDetails)
                    .then(function(response){
                        if(response.data.error){
                            logger.error('Could not place the order; please try again: ' + response.data.message);
                        }
                        else{
                            logger.success('Your order has been placed');
                        }
                    })
                    .catch(function(err){
                        logger.error('Could not place the order; please try again: ' + err.data.message);
                    });
            };

            // endregion

            // region Events

            pubSub.on('login:success', this.moveToNextStep.bind(this, true));

            // endregion

        }

        return {
            restrict: 'AE',
            templateUrl: '/js/app/components/checkoutPage/checkoutPage.html',
            scope: {},
            controller: [
                'accordionService',
                'authService',
                'ecommerceDataService',
                'shoppingCartService',
                'pubSubService',
                'logger',
                checkoutPageCtrl
            ],
            controllerAs: 'vm',
            bindToController: true
        };
    }

    // region CommonJS

    module.exports = {
        name: 'checkoutPage',
        directive: [checkoutPageDirective]
    };

    // endregion

 })();