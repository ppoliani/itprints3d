/**
 * service that executes xhr requests to the back-end
 */
(function(){
    'use strict';

    function resourceService($resource, SERVICE_ENDPOINT){

        // region Consts

        var API_URL = 'http://localhost:9090/api/v1/';

        // endregion

        // region Inner Methods

        /**
         * Executes an http GET request
         * @param url
         */
        function get(url){
            throw 'Not Implemented';
        }

        /**
         * Executes an http POST request
         * @param url
         * @param data
         */
        function post(url, data){
            throw 'Not Implemented';
        }

        /**
         * Executes an http PUT request
         * @param url
         * @param data
         */
        function put(url, data){
            throw 'Not Implemented';
        }

        /**
         * Gets the path for the given resource
         * @param resouce
         * @returns {string}
         * @private
         */
        function _getPath(resource){
            return SERVICE_ENDPOINT + resource;
        }

        // endregion

        // region public API

        return {
            get: get,
            post: post,
            put: put,
            apiUrl: API_URL
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'resourceService',
        type: 'factory',
        service: [
            '$resource',
            'SERVICE_ENDPOINT',
            resourceService
        ]
    };

    // endregion
})();

