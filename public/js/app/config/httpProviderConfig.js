/**
 * httpProvider configurations
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Inner Methods

    /**
     * http interceptor that adds the bearer token.
     */
    function oauth2Interceptor($q, $window){
        return {
            request: function(config){
                config.headers = config.headers || {};

                if($window.sessionStorage.access_token){
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
                }

                return config;
            },

            response: function (response) {
                if (response.status === 401) {
                    // handle the case where the user is not authenticated
                    throw 'User not authenticated';
                }
                return response || $q.when(response);
            }
        };
    }

    /**
     * Runs the configurations
     */
    function configure(app){
        app.factory('oauth2Interceptor', ['$q', '$window', oauth2Interceptor]);

        app.config(['$httpProvider', function($httpProvider){
            $httpProvider.interceptors.push('oauth2Interceptor');
        }]);
    }

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

module.exports = httpProviderConfig;

// endregion