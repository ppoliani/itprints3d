/**
 * Defines callbacks for the route events
 */
var routeCallbackConfig = (function(){
    'use strict';

    function configure (app){
        app.run(['$rootScope', '$window', '$state', '$timeout', 'breezeService', 'routeAuthService', 'authService', 'pubSubService', function($rootScope, $window, $state, $timeout, breezeService, routeAuthService, authService, pubsub){
            $rootScope.$on('$stateChangeStart', function(event, toState){
                if(!breezeService.metadataLoaded) {
                    event.preventDefault();

                    // prevent state transition if metadata not loaded
                    var metadataLoaded = function () {
                        if (breezeService.metadataLoaded) {
                            $state.transition(toState.name)
                        } else {
                            $timeout(metadataLoaded, 0);
                        }
                    };

                    $timeout(metadataLoaded, 0);
                }

                // if user unauthorized
                if(!routeAuthService.canVisitRoute(toState)){
                    event.preventDefault();
                    $state.transition('home');
                }

                if(toState.name === 'login'){
                    authService.checkIfLoggedIn();
                }
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState){
                $rootScope.title = 'ItPrints3d | ' + toState.name;

                // publish breadcrumb update
                var items = toState.url.split('/').reduce(function(prev, current){
                    if(!current) { return prev; }

                    prev.push({ name: current, state: current });
                    return prev;
                }, []);

                pubsub.publish('breadcrumbs:update', items);
            });
        }]);
    }

    return {
        configure: configure
    };
})();

// region CommonJS

module.exports = routeCallbackConfig;

// endregion