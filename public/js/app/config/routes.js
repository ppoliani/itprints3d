/**
 * App entry point
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Consts

    // This is relative to the nodeJS static files directory !!!
    var BASE_DIR = '/js/app/';

    // endregion

    // region Inner Methods

    /**
     * Runs the configurations
     */
    function configure(app){
        app.config(['$stateProvider',
            '$urlRouterProvider',
            '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: _getPath('home/index'),
                        controller: 'homeCtrl as  vm'
                    })

                    .state('login', {
                        url: '/login',
                        templateUrl: _getPath('auth/login'),
                        controller: 'authCtrl as vm'
                    })

                    .state('printers', {
                        url: '/printers',
                        templateUrl: _getPath('printers/printers')
                    })

                    .state('printer', {
                        url: '/printers/:id',
                        template: '<product-page product-type="Printer"></product-page>'
                    })

                    .state('shoppingCart', {
                        url: '/cart',
                        templateUrl: _getPath('e-commerce/shoppingCart')
                    })

                    .state('checkout', {
                        url: '/checkout',
                        template: '<checkout-page></checkout-page>'
                    })

                    .state('people', {
                        url: '/people',
                        templateUrl: _getPath('tmp/people'),
                        controller: 'peopleCtrl as vm',
                        claims: {
                            role: ['admin']
                        }
                    });

                $urlRouterProvider.otherwise('/');
                $locationProvider.html5Mode(true);
            }]);
    }

    /**
     * Return the paths relative to base dir
     *
     * @param filePath
     * @returns {string}
     * @private
     */
    function _getPath(filePath){
        return BASE_DIR + filePath + '.html';
    }

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

module.exports = httpProviderConfig;

// endregion