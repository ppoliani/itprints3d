/**
 * Home Controller
 */
(function(){
    'use strict';

    // region Controller

    function homeCtrl(){
        //region Inner Fields

        // endregion


        // region Viewmodel

        // endregion
    }

    // endregion

    // region CommonJS

    module.exports = {
        name: 'homeCtrl',
        ctrl: [homeCtrl]
    };

    // endregion
})();

