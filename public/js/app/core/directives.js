/**
 * Loads all the directives
 */
module.exports = [
    require('../components/preloadResource/preloadResource'),
    require('../components/breezeInit/breezeInit'),
    require('../components/homeSlider/homeSlider'),
    require('../components/criticalResource/criticalResource'),
    require('../components/productGrid/productGrid'),
    require('../components/breadcrumbs/breadcrumbs'),
    require('../components/pagination/pagination'),
    require('../components/imageMagnifier/imageMagnifier'),
    require('../components/carousel/carouselContainer'),
    require('../components/carousel/carouselItem'),
    require('../components/accordion/accordionContainer'),
    require('../components/accordion/accordionItem'),
    require('../components/productImageGallery/productImageGallery'),
    require('../components/counter/counter'),
    require('../components/productActions/productActions'),
    require('../components/rating/rating'),
    require('../components/validationError/validationError'),
    require('../components/tabs/tabContainer'),
    require('../components/tabs/tabItem'),
    require('../components/shoppingCart/shoppingCart'),
    require('../components/productPage/productPage'),
    require('../components/shoppingCartWidget/shoppingCartWidget'),
    require('../components/checkoutPage/checkoutPage')
];