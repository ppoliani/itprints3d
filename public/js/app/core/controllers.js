/**
 * Loads all the controllers of the app
 */
module.exports = [
    require('../partial/header/headerCtrl'),
    require('../home/homeCtrl'),
    require('../auth/authCtrl'),
    require('../partial/sidebar/sidebarCtrl'),
    require('../partial/productQuestion/productQuestionCtrl')
];