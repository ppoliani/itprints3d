/**
 * Contains all the value services that will be used in the app
 */
module.exports = [{
    name: 'alertify',
    type: 'value',
    service: alertify
}, {
    name: 'accounting',
    type: 'value',
    service: accounting
}, {
    name: 'Event',
    type: 'value',
    service: Event
}, {
    name: 'Magnifier',
    type: 'value',
    service: Magnifier
}];