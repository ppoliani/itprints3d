/**
 * Loads all the filters of the app
 */
module.exports = [
    require('../filters/priceFilter'),
    require('../filters/unsafeHtmlFilter')
];