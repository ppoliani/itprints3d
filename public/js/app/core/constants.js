/**
 * Contains all the constants
 */
module.exports = [{
    name: 'SERVICE_ENDPOINT',
    type: 'constant',
    service: '@@SERVICE_ENDPOINT'
}, {
    name: 'SIGNUP_ENDPOINT',
    type: 'constant',
    service: '@@SIGNUP_URL'
},{
    name: 'TOKEN_URL',
    type: 'constant',
    service: '@@TOKEN_URL'

}, {
    name: 'IMAGE_URL',
    type: 'constant',
    service: '@@IMAGE_URL'
}, {
    name: 'PRODUCT_IMAGE_URL',
        type: 'constant',
        service: '@@PRODUCT_IMAGE_URL'
}, {
    name: 'ORDER_BY_OPTIONS',
    type: 'constant',
    service: {
        'Price: Lowest first': 'price',
        'Price: Highest first': '-price',
        'Price: Product Name: A to Z': 'productName',
        'Price: Product Name: Z to A': '-productName'
    }
},  {
    name: 'IMAGE_PROP_SUFFIX',
    type: 'constant',
    service: '_ImageID'
}];