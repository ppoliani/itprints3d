/**
 * Loads all the services of the app
 */
module.exports = [
    require('../auth/authService'),
    require('../auth/routeAuthService'),
    require('../dataServices/resourceService'),
    require('../utils/loggerProvider'),
    require('../utils/pubsub'),
    require('../utils/imgUriBuilder'),

    // Data
    require('../data/breezeService'),
    require('../data/jsonResultsAdapterService'),
    require('../data/models/modelSet'),
    require('../data/repositoryQueryService'),
    require('../data/repositoryService'),
    require('../data/repositoriesService'),
    require('../data/unitOfWorkService'),

    require('../e-commerce/shoppingCartService'),
    require('../e-commerce/ecommerceDataService'),
    require('../components/accordion/accordionService')
];