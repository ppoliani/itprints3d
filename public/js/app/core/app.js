/**
 * App routes
 */
var app = (function(){
    'use strict';

    // region Deps

    var
        angular = require('angular'),
        httpProviderConfig = require('./../config/httpProviderConfig'),
        routes = require('../config/routes'),
        routeCallbackConfig = require('../config/routeCallbacks'),
        controllers = require('./controllers'),
        services = require('./services'),
        values = require('./values'),
        constants = require('./constants'),
        filters = require('./filters'),
        models = require('./models'),
        modelSet = require('../data/models/modelSet'),
        directives = require('./directives');

    // endregion

    // region Private Fields

    var mainModule = angular.module('app.core', [
            'ui.router',
            'breeze.angular',
            'ngAnimate',
            'ngDialog',
            'angular-loading-bar',
            'app.services',
            'app.controllers',
            'app.filters',
            'app.models',
            'app.directives'
        ]),
        servicesModule = angular.module('app.services', []),
        controllersModule = angular.module('app.controllers', []),
        filtersModule = angular.module('app.filters', []),
        modelsModule = angular.module('app.models', []),
        directivesModule = angular.module('app.directives', []);

    // endregion

    // region Register all Controllers

    controllers.forEach(function(controller){
        controllersModule.controller(controller.name, controller.ctrl);
    });

    // endregion

    // region Register All Services

    services.concat(values).concat(constants).forEach(function(service){
        servicesModule[service.type](service.name, service.service);
    });

    // endregion

    // region Register All Filters

    filters.forEach(function(service){
        filtersModule[service.type](service.name, service.filter);
    });

    // endregion

    // region Register All Models

    models.forEach(function(model){
        modelsModule.factory(model.name, model.ctor);
    });

    modelsModule.value(modelSet);

    // endregion

    // region Register All Directives

    directives.forEach(function(directive){
        directivesModule.directive(directive.name, directive.directive);
    });

    // endregion

    // region Config Phase

    httpProviderConfig.configure(mainModule);
    routes.configure(mainModule);
    routeCallbackConfig.configure(mainModule);

    servicesModule.config(['loggerProvider', function(loggerProvider){
        loggerProvider.init({ debugMode: true });
    }]);

    // endregion

    // region Run Phase


    // endregion

    // region Public API

    return mainModule;

    // endregion

})();

// region CommonJS

module.exports = app;

// endregion