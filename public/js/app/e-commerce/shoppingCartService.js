/**
 * The shopping cart service
 */
 (function(){
    'use strict';

    function shoppingCartService($q, logger, pubsub, ecommerceDataService){

        // region Inner Fields

        var _shoppingCart = null;

        // endregion

        // region Inner Methods

        function load(){
            return ecommerceDataService.getShoppingCart()
                .then(on_response)
                .catch(on_error.bind('fetching'));
        }

        function add(product, productType, quantity){
            if(!product.id){
                throw new Error("ProductID not defined");
            }

            return ecommerceDataService.addProductToShoppingCart(_getAddToCartData(product, productType, quantity))
                .then(on_response)
                .catch(on_error.bind('adding a product to the shopping cart: '));
        }

        function remove(product){
            if(!product.id){
                throw new Error("ProductID not defined");
            }

            return ecommerceDataService.removeProductFromShoppingCart(product.productID, _shoppingCart.id)
                .then(on_response)
                .catch(on_error.bind('removing a product from the shopping cart: '));
        }

        function update(){
            if(!_shoppingCart){
                throw new Error("Cannot update an empty shopping cart");
            }

            return ecommerceDataService.updateShoppingCart(_shoppingCart)
                .then(on_response)
                .catch(on_error.bind('updating the shopping cart: '));
        }

        function getShoppingCart(){
            return $q(function(resolve, reject){
                if(!_shoppingCart){
                    load()
                        .then(function(){
                            resolve(_shoppingCart)
                        })
                        .catch(reject);
                }
                else {
                    resolve(_shoppingCart);
                }
            });
        }

        function getProductsCount(){
            return getShoppingCart()
                .then(function(){
                    return _shoppingCart
                        ? _shoppingCart.cartProducts.reduce(function(prev, product){
                                prev += product.quantity;
                                return prev;
                            }, 0)
                        : 0;
                });
        }

        function getProductsTotalPrice(){
            return getShoppingCart()
                .then(function(){
                    return _shoppingCart
                        ? _shoppingCart.cartProducts.reduce(function(prev, product){
                                prev += product.quantity * product.price;
                                return prev;
                            }, 0)
                        : 0;
                });
        }

        function clear(){
            return ecommerceDataService.removeShoppingCart(_shoppingCart.id)
                .then(function(){
                    _shoppingCart = null;
                })
                .catch(on_error.bind('a product to the shopping cart: '));
        }

        function _getAddToCartData(product, productType, quantity){
            return {
                productID: product.id,
                category: productType,
                quantity: quantity
            };
        }

        function _broadcastUpdate(){
            pubsub.publish('shoppingCart:update');
        }


        // endregion

        // region Events

        function on_response(response){
            if(response.data.error){
                logger.error(response.data.message);
            }
            else {
                _shoppingCart = response.data;

                _broadcastUpdate();
            }

            return _shoppingCart;
        }

        function on_error(msg, err){
            logger.error('Error while' + msg + err);
        }

        // endregion

        // region Public API

        return {
            load: load,
            add: add,
            remove: remove,
            update: update,
            getShoppingCart: getShoppingCart,
            getProductsCount: getProductsCount,
            getProductsTotalPrice: getProductsTotalPrice,
            clear: clear
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'shoppingCartService',
        type: 'factory',
        service: [
            '$q',
            'logger',
            'pubSubService',
            'ecommerceDataService',
            shoppingCartService
        ]
    };

    // endregion

 })();