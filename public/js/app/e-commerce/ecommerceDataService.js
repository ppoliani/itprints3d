/**
 * Executes http call related to the e-commerce module
 */
 (function(){
    'use strict';

    function ecommerceDataService($http, SERVICE_ENDPOINT, logger){

        // region Inner Methods

        function getShoppingCart(){
            return $http.get(_getPath('shopping-cart'));
        }

        function addProductToShoppingCart(product){
            var data = {
                product: product
            };

            return $http.post(_getPath('shopping-cart'), data);
        }

        function removeShoppingCart(cartID){
            return $http.delete(_getPath('shopping-cart/' + cartID));
        }

        function removeProductFromShoppingCart(productID, cartID){
            return $http.delete(_getPath('shopping-cart/' + cartID + '/' + productID));
        }

        function updateShoppingCart(shoppingCart){
            return $http.put(_getPath('shopping-cart/' + shoppingCart.id), { products: shoppingCart.cartProducts });
        }

        function checkout(shoppingCartId){
            return $http.post(_getPath('checkout'), { cartID: shoppingCartId });
        }

        function placeOrder(orderDetails){
            return $http.post(_getPath('pay'), orderDetails);
        }

        function _getPath(endpoint){
            return SERVICE_ENDPOINT + endpoint;
        }

        // endregion

        // region Public API

        return {
            getShoppingCart: getShoppingCart,
            addProductToShoppingCart: addProductToShoppingCart,
            removeShoppingCart: removeShoppingCart,
            removeProductFromShoppingCart: removeProductFromShoppingCart,
            updateShoppingCart: updateShoppingCart,
            checkout: checkout,
            placeOrder: placeOrder
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'ecommerceDataService',
        type: 'factory',
        service: [
            '$http',
            'SERVICE_ENDPOINT',
            'logger',
            ecommerceDataService
        ]
    };

    // endregion

 })();