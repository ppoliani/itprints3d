describe('The shopping cart service', function(){

    var $q,
        $rootScope,
        shoppingCartService,
        product, cartEntityResponse;

    // mock/stubs
    var ecommerceDataServiceStub = {
        getShoppingCart: sinon.stub(),
        addProductToShoppingCart: sinon.stub(),
        removeShoppingCart: sinon.stub(),
        removeProductFromShoppingCart: sinon.stub(),
        updateShoppingCart: sinon.stub()
    };

    function addProduct(){
        ecommerceDataServiceStub.addProductToShoppingCart.returns(utils.resolvePromise($q, cartEntityResponse));
        return shoppingCartService.add(product);
    }

    beforeEach(function(){
        angular.mock.module(utils.moduleName, function($provide){
            $provide.value('ecommerceDataService', ecommerceDataServiceStub);
        });
    });

    beforeEach(inject(function(_$rootScope_, _$q_, _shoppingCartService_){
        $rootScope = _$rootScope_;
        $q = _$q_;
        shoppingCartService = _shoppingCartService_;

        product = {
            quantity: 1,
            category: 'Printer',
            id: '1234'
        };

        cartEntityResponse = {
            data: {
                id: 'cart1234',
                cartProducts: [{
                    productID: '1234',
                    productName: 'Product name',
                    quantity: 1
                }]
            }
        };
    }));

    var utils = require('../utils');

    describe('The load method', function(){
        it('Should get the shopping for the current user', function(done){
            // Arrange
            ecommerceDataServiceStub.getShoppingCart.returns(utils.resolvePromise($q, cartEntityResponse));

            // Act
            shoppingCartService.load();

            // Assert
            shoppingCartService.load(product)
                .then(function(){
                    // Assert
                    shoppingCartService.getShoppingCart()
                        .then(function(shoppingCart){
                            expect(shoppingCart).not.to.be.null;
                            expect(shoppingCart.cartProducts[0].productID).to.equal('1234');

                            done();
                        });
                });

            $rootScope.$apply();
        });
    });

    describe('The add method', function(){
        it('Should add a product to the shopping cart', function(done){
            // Arrange
            ecommerceDataServiceStub.addProductToShoppingCart.returns(utils.resolvePromise($q, cartEntityResponse));

            // Act
            shoppingCartService.add(product)
                .then(function(){
                    // Assert
                    shoppingCartService.getShoppingCart()
                        .then(function(shoppingCart){
                            expect(shoppingCart).not.to.be.null;
                            expect(shoppingCart.cartProducts[0].productID).to.equal('1234');

                            done();
                        });
                });

            $rootScope.$apply();
        });

        it('Should not add a product to the shopping cart if there is a problem with the back-end', function(done){
            // Arrange
            ecommerceDataServiceStub.addProductToShoppingCart.returns(utils.rejectPromise($q));
            ecommerceDataServiceStub.getShoppingCart.returns(utils.resolvePromise($q, { data: {} }));

            // Act
            shoppingCartService.add(product)
                .then(function(){
                    // Assert
                    shoppingCartService.getShoppingCart()
                        .then(function(shoppingCart){
                            expect(shoppingCart).not.to.be.null;

                            done();
                        });
                });

            $rootScope.$apply();
        });
    });

    describe('The remove method', function(){
        it('Should remove the product from the list', function(done){
            // Arrange
            cartEntityResponse.data.cartProducts[0] = null;

            addProduct().then(function(){
                ecommerceDataServiceStub.removeProductFromShoppingCart.returns(utils.resolvePromise($q, cartEntityResponse));

                // Act
                expect(shoppingCartService.getShoppingCart()).not.to.be.null;

                shoppingCartService.remove(product)
                    .then(function(){
                        // Assert
                        shoppingCartService.getShoppingCart()
                            .then(function(shoppingCart){
                                expect(shoppingCart.cartProducts[0]).to.be.null;

                                done();
                            });
                    });
            });

            $rootScope.$apply();
        });
    });

    describe('The clear method', function(){
        it('Should remove the shopping cart', function(done){
            // Arrange
            var cartData = {};

            addProduct().then(function(){
                ecommerceDataServiceStub.removeShoppingCart.returns(utils.resolvePromise($q, {}));
                ecommerceDataServiceStub.getShoppingCart.returns(utils.resolvePromise($q, { data: cartData }));

                // Act
                expect(shoppingCartService.getShoppingCart()).not.to.be.null;

                shoppingCartService.clear(cartEntityResponse.data.id)
                    .then(function(){
                        // Assert
                        shoppingCartService.getShoppingCart()
                            .then(function(shoppingCart){
                                expect(shoppingCart).to.equal(cartData);

                                done();
                            });
                    });
            });

            $rootScope.$apply();
        });
    });

    describe('The update method', function(){
        it('Should update the shopping cart', function(done){
            // Arrange
            cartEntityResponse.data.cartProducts[0].quantity = 2;

            addProduct().then(function(){
                ecommerceDataServiceStub.updateShoppingCart.returns(utils.resolvePromise($q, cartEntityResponse));

                // Act
                expect(shoppingCartService.getShoppingCart()).not.to.be.null;

                shoppingCartService.update()
                    .then(function(){
                        // Assert
                        shoppingCartService.getShoppingCart()
                            .then(function(shoppingCart){
                                expect(shoppingCart.cartProducts[0].quantity).to.equal(2);

                                done();
                            });
                    });
            });

            $rootScope.$apply();
        });
    });
});