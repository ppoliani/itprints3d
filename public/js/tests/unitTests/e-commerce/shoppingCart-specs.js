
describe('The shopping cart component', function(){
    var $compile,
        shoppingCartService,
        $q, scope, element,
        $state,

        shoppingCartServiceStub = {
            getShoppingCart: sinon.stub(),
            clear: sinon.stub(),
            update: sinon.stub()
        },

        $stateStub = {
            go: sinon.stub()
        },

        shoppingCartContent = {
            data: {
                id: 'cart1234',
                cartProducts: [{
                    productName: 'Product A',
                    quantity: 2,
                    price: 1
                }, {
                    productName: 'Product B',
                    quantity: 1,
                    price: 10
                }, {
                    productName: 'Product C',
                    quantity: 2,
                    price: 20
                }]
            }
        };

    beforeEach(function(){
        angular.mock.module(utils.moduleName, function($provide){
            $provide.value('shoppingCartService', shoppingCartServiceStub);
            $provide.value('$state', $stateStub);
        });
    });

    beforeEach(inject(function($rootScope, _$compile_, _$q_){
        $compile = _$compile_;
        $q = _$q_;

        shoppingCartServiceStub.getShoppingCart.returns(utils.resolvePromise($q, shoppingCartContent.data));

        scope = $rootScope.$new();
        element = $compile('<shopping-cart></shopping-cart>')(scope);
        scope.$digest();
    }));

    var utils = require('../utils');

    describe('The controller', function(){
        it('Should get the shopping cart from the shopping cart service', function(){
            // Expect
            expect(shoppingCartServiceStub.getShoppingCart).to.have.been.called;
        });
    });

    describe('The getSubtotal method', function(){
        it('Should find the correct subtotal for the given product', function(){
            // Arrange
            var product = shoppingCartContent.data.cartProducts[0];

            // Act
            var result = scope.vm.getSubtotal(product);

            // Assert
            expect(result).to.equal(2);
        });
    });

    describe('The continueShopping methods', function(){
        it('Should redirect to the home page', function(){
            // Act
            scope.vm.continueShopping();

            // Assert
            expect($stateStub.go).to.have.been.called;
        });
    });

    describe('The clear method', function(){
        it('Should empty the product collection', function(){
            // Act
            expect(scope.vm.products).to.have.length.above(0);
            scope.vm.clear();

            // Assert
            expect(scope.vm.products).to.have.length(0);
        });

        it('Should call the clear method of the shoppingCartService', function(){
            // Act
            scope.vm.clear();

            // Assert
            expect(shoppingCartServiceStub.clear).to.have.been.called;
        });
    });

    describe('The update method', function(){
        it('Should call the update method of the shoppingCartService', function(){
            // Arrange
            shoppingCartServiceStub.update.returns(utils.resolvePromise($q, shoppingCartContent.data));

            // Act
            scope.vm.update();

            // Assert
            expect(shoppingCartServiceStub.update).to.have.been.called;
        });
    });
});