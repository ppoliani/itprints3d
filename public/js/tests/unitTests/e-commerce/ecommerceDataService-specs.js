describe('The ecommerceDataService service', function(){

    var $httpBackend,
        ecommerceDataService;

    beforeEach(function(){
        angular.mock.module(utils.moduleName);
    });

    beforeEach(inject(function(_$httpBackend_, _ecommerceDataService_){
        $httpBackend = _$httpBackend_;
        ecommerceDataService = _ecommerceDataService_;
    }));

    var utils = require('../utils');

    describe('The loadShoppingCart method', function(){
       it('Should send a get request to the shopping cart endpoint', function(){
           // Arrange
           $httpBackend
               .expectGET(utils.getPath('shopping-cart'))
               .respond(200);

           // Act
           ecommerceDataService.getShoppingCart()
               .then(function(){
                   expect(true).to.be.true;
               });
       });
    });

    describe('The addProductToShoppingCart method', function(){
        it('Should post data to the shopping-cart endpoint', function(){
            // Arrange
            var cart = {
                    cartProducts: []
                };

            $httpBackend
                .expectPOST(utils.getPath('shopping-cart'), cart)
                .respond(200);

            // Act
            ecommerceDataService.addProductToShoppingCart({})
                .then(function(response){
                    // Assert
                    expect(response).to.equal(cart);
                });
        });
    });

    describe('The removeShoppingCart method', function(){
        it('Should delete a shopping cart by calling the delete shopping-cart endpoint', function(){
            // Arrange
            $httpBackend
                .expectDELETE(utils.getPath('shopping-cart/cartid'))
                .respond(200);

            // Act
            ecommerceDataService.removeShoppingCart({})
                .then(function(){
                    // Assert
                    expect(true).to.be.true;
                });
        });
    });

    describe('The removeProductFromShoppingCart method', function(){
        it('Should delete a product by calling the shopping-cart endpoint', function(){
            // Arrange
            $httpBackend
                .expectDELETE(utils.getPath('shopping-cart/cartid/productId'))
                .respond(200);

            // Act
            ecommerceDataService.removeProductFromShoppingCart({})
                .then(function(){
                    // Assert
                    expect(true).to.be.true;
                });
        });
    });

    describe('The updateShoppingCart method', function(){
        it('Should put the updated cart', function(){
            // Arrange
            var cart = {
                    cartProducts: []
                };

            $httpBackend
                .expectPUT(utils.getPath('shopping-cart'), cart)
                .respond(200);

            // Act
            ecommerceDataService.updateShoppingCart({})
                .then(function(response){
                    // Assert
                    expect(response).to.equal(cart);
                });
        });
    });
});